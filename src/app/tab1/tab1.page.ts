import {Component} from '@angular/core';
import {Platform, ActionSheetController} from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  transicion = false;

  transicionSeleccionada: string;

  opcionesDeAnimacion = [
    'animate__rollIn',
    'animate__backInDown',
    'animate__fadeInDownBig',
    'animate__fadeInLeftBig',
    'animate__fadeInTopLeft',
    'animate__fadeInBottomLeft',
    'animate__flip',
    'animate__flipInX',
    'animate__lightSpeedInRight',
    'animate__rotateInDownLeft',
    'animate__rollIn',
    'animate__zoomInDown',
  ];

  constructor(
    private readonly actionSheetController: ActionSheetController
  ) {
  }

  async abirMenu() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      backdropDismiss: true,
      buttons: [{
        text: 'Opcion 1',
        icon: 'share',
        handler: () => {
          this.transicionSeleccionada = this.opcionesDeAnimacion[0];
        }
      },
        {
          text: 'Opcion 2',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[1];
          }
        },
        {
          text: 'Opcion 3',
          icon: 'caret-forward-circle',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[2];
          }
        },
        {
          text: 'Opcion 4',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[3];
          }
        },
        {
          text: 'Opcion 5',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[4];
          }
        },
        {
          text: 'Opcion 6',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[5];
          }
        },
        {
          text: 'Opcion 7',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[6];
          }
        },
        {
          text: 'Opcion 8',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[7];
          }
        },
        {
          text: 'Opcion 9',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[8];
          }
        },
        {
          text: 'Opcion 10',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[9];
          }
        },
        {
          text: 'Opcion 11',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[10];
          }
        },
        {
          text: 'Opcion 12',
          icon: 'share',
          handler: () => {
            this.transicionSeleccionada = this.opcionesDeAnimacion[11];
          }
        },
        {
          text: 'Cancelar',
          icon: 'share',
          role: 'cancel',
          handler: () => {
          }
        }]
    });
    await actionSheet.present();
  }

  cambiarTransicion() {
    this.transicion = !this.transicion;
  }
}
